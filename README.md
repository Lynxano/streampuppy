hey yall  
this is a modified and expanded version of my personal stream manager, which in of itself is a command line version of a larger gui stream manager i made a while ago

# requirements
 -	python-twitch-client:  

		pip install python-twitch-client  
 

 -	you will need to have a twitch client id, which can be found at the app page of your twitch  
 twitch dev console, if you dont have an app set up you can make one assuming you already have a twitch account  
 go [here](https://dev.twitch.tv/console/apps) to set that up !!! sign in and make an app and then click  
 manage on it to see the client id



## for voice assistant  
 enable voice settings by uncommenting the code with them in it and playign with it to suit your needs  
 make sure to use simple phrases

 -	speach reognition:  

		pip3 install SpeechRecognition  


 -	pyaduio:  

		pip3 install pyaudio  


 -	pocket sphinx:  

		pip3 install pocketsphinx


### obs  
 to have the follower information read in obs, you can either have it capture the follower  
 the follower terminal window, or use the text insert, which, from my expierence, is the more  
 portable and versatile solution

 ![a setting titled read from file under text properties in open broadcaster software](/images/readfile.png)
 

uuwu
#### to do  
 -	make voice assistant work on its more smoothly and better integrated

 -	make binaries for linux arm64 and x86 64 win10 bcus it sounds fun idk (and make guide for people who want to make their own)

 -	add more api bindings